using aspnetcoreapp.Models;
using AspnetCoreApp.Models;
using Microsoft.EntityFrameworkCore;

namespace aspnetcoreapp.DataBase
{
    public class VegaDbContext : DbContext
    {
       public VegaDbContext(DbContextOptions<VegaDbContext> options)
            :base(options) 
        { 

        } 

        public DbSet<Make> Makes { get; set; }
        public DbSet<Feature> Features { get; set; }
        

    }
}