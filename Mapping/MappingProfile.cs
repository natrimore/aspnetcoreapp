using aspnetcoreapp.Controllers.Resources;
using aspnetcoreapp.Models;
using AspnetCoreApp.Models;
using AutoMapper;

namespace aspnetcoreapp.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Make, MakeResource>();
            CreateMap<Model, ModelResource>();
            CreateMap<Feature, FeatureResource>();
        }
    }
}